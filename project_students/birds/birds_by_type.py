from project_students.birds.bird_type import BirdType

birds_by_type = {
    BirdType.FINCH:
        {"Chaffinch": ['chaffinch.jpg', 'chaffinch.mp3'],
         "Bullfinch": ['bullfinch.jpg', 'bullfinch.mp3']},
    BirdType.ACCIPITRINAE:
        {"Pern": ['pern.jpg', 'pern.mp3']},
    BirdType.THRUSH:
        {"Blackbird": ['blackbird.jpg', 'blackbird.mp3'],
         "Redwing": ['redwing.jpg', 'redwing.mp3'],
         "Fieldfare": ['fieldfare.jpg', 'fieldfare.mp3'],
         "Song thrush": ['songthrush.jpg', 'songthrush.mp3']},
    BirdType.CORVID:
        {"Hooded crow": ['crow.jpg', 'crow.mp3'],
         "Magpie": ['magpie.jpg', 'magpie.mp3'],
         "Siberian jay": ['siberianjay.jpg', 'siberianjay.mp3'],
         "Jay": ['jay.jpg', 'jay.mp3'],
         "Rook": ['rook.jpg', 'rook.mp3']}
}
