from enum import Enum, auto


class BiotopeType(Enum):
    FOREST = auto()
    FIELD = auto()
    CITY = auto()
    SWAMP = auto()


    @classmethod
    def min_value(cls):
        return list(cls)[0].value

    @classmethod
    def max_value(cls):
        return list(cls)[-1].value


biotope_by_bird = {
    "Chaffinch": {BiotopeType.FOREST, BiotopeType.FIELD},
    "Bullfinch": {BiotopeType.FOREST},
    "Fieldfare": {BiotopeType.FIELD},
    "Hooded crow": {BiotopeType.CITY}


}
if __name__ == '__main__':
    B = BirdType(BirdType.FINCH)
    print(B)
    print(B.name)
    print(len(BirdType))
    print(list(BirdType)[-1])
    print(BirdType.max_value())
    print(BirdType.min_value())


