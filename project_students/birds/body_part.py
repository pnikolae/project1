from enum import Enum, auto

#точки защиты


class BodyPart(Enum):
    NOTHING = auto()
    HEAD = auto()
    BELLY = auto()
    LEGS = auto()

    @classmethod
    def min_value(cls):
        return list(cls)[0].value

    @classmethod
    def max_value(cls):
        return list(cls)[-1].value

    @classmethod
    def has_item(cls, name: str):
        return name in cls._member_names_
