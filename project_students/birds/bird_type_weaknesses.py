from project_students.birds.bird_type import BirdType

# в tuple указаны категории, перед которыми есть уязвимости
bird_defence_weaknesses_by_type = {
    BirdType.FINCH: (BirdType.ACCIPITRINAE, ),
    BirdType.ACCIPITRINAE: (BirdType.CORVID, )
}