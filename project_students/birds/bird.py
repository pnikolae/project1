from project_students.birds.bird_type import BirdType
from project_students.birds.bird_type_weaknesses import bird_defence_weaknesses_by_type as weaknesses_by_type
from project_students.birds.body_part import BodyPart
from project_students.birds.player_state import PlayerState
from project_students.birds.biotope_type import BiotopeType, biotope_by_bird


class Bird:
    def __init__(self,
                 name: str,
                 bird_type: BirdType):
        self.name = name
        self.bird_type = bird_type
        self.weaknesses = weaknesses_by_type.get(bird_type, tuple())
        self.hp = 100
        self.attack_point = None
        self.defence_point = None
        self.hit_power = 20  # 5
        self.state = PlayerState.READY
        self.biotope = biotope_by_bird.get(name, set())

    def __str__(self):
        return f"Name: {self.name} | Type: {self.bird_type.name}\nHP: {self.hp}"

    def next_step_points(self,
                         next_attack: BodyPart,
                         next_defence: BodyPart):
        self.attack_point = next_attack
        self.defence_point = next_defence

    def get_hit(self,
                opponent_attack_point: BodyPart,
                opponent_hit_power: int,
                opponent_type: BirdType,
                game_location: BiotopeType):
        if opponent_attack_point == BodyPart.NOTHING:
            return "get_hit_thanks"  # "Спасибо :)"
        elif self.defence_point == opponent_attack_point:
            return "get_hit_miss"  # "Слабак!"
        else:
            self.hp -= opponent_hit_power * (3 if opponent_type in self.weaknesses else 1) * (0.5 if game_location in self.biotope else 1)

            if self.hp <= 0:
                self.state = PlayerState.DEFEATED
                return "get_hit_defeated"  # "Побежден :("
            else:
                return "get_hit_injured"  # "Ранен, но не побежден!"


if __name__ == '__main__':
    p1 = Bird('ololol', BirdType.FINCH)
    print(p1)
