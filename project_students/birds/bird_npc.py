import random

from project_students.birds.bird import Bird
from project_students.birds.bird_type import BirdType
from project_students.birds.birds_by_type import birds_by_type
from project_students.birds.body_part import BodyPart


class BirdNPC(Bird):
    def __init__(self):
        rand_type_value = random.randint(BirdType.min_value(), BirdType.max_value())
        rand_bird_type = BirdType(rand_type_value)

        rand_bird_name = random.choice(list(birds_by_type.get(rand_bird_type, {}).keys()))

        super().__init__(rand_bird_name, rand_bird_type)

    # для избежания проблем связанных с наследованием добавим аргумент заглушку **kwargs
    # или можно переименовать метод
    # или игнорировать warning
    def next_step_points(self, **kwargs):
        attack_point = BodyPart(random.randint(BodyPart.min_value(), BodyPart.max_value()))
        defence_point = BodyPart(random.randint(BodyPart.min_value(), BodyPart.max_value()))
        super().next_step_points(next_attack=attack_point,
                                 next_defence=defence_point)


if __name__ == '__main__':
    bird_npc = BirdNPC()
    bird_npc.next_step_points()
    print(bird_npc)
