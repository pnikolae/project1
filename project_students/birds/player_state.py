from enum import Enum, auto


class PlayerState(Enum):
    READY = auto()
    DEFEATED = auto()
