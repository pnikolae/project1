from enum import Enum, auto


class BirdType(Enum):
    ACCIPITRINAE = auto()
    FINCH = auto()
    THRUSH = auto()
    CORVID = auto()


    @classmethod
    def min_value(cls):
        return list(cls)[0].value

    @classmethod
    def max_value(cls):
        return list(cls)[-1].value


if __name__ == '__main__':
    B = BirdType(BirdType.FINCH)
    print(B)
    print(B.name)
    print(len(BirdType))
    print(list(BirdType)[-1])
    print(BirdType.max_value())
    print(BirdType.min_value())


