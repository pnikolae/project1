import json
from math import ceil, sqrt
import random

import telebot
from telebot import types
from telebot.types import Message

from project_students.birds.body_part import BodyPart
from project_students.birds.game_result import GameResult
from project_students.birds.bird import Bird
from project_students.birds.bird_npc import BirdNPC
from project_students.birds.birds_by_type import birds_by_type
from project_students.birds.player_state import PlayerState
from project_students.birds.bird_type import BirdType
from project_students.birds.biotope_type import BiotopeType

try:
    with open("./bot_token.txt", 'r') as token_file:
        token = token_file.read().strip()
except FileNotFoundError as my_error:
    print("Создайте файл bot_token.txt с токеном своего бота!")
    raise SystemExit

bot = telebot.TeleBot(token)

# pnikolae 08.06.2021
US = '\U0001F1FA\U0001F1F8'
RU = '\U0001F1F7\U0001F1FA'
lang = "ru"  # "df" for default
location = None
try:
    with open("./data.json", 'r', encoding='utf-8') as msg_file:
        msg_dict = json.load(msg_file)  # msg_dict[lang][msg_name]
except FileNotFoundError as my_error:
    msg_dict = {}
    print("Будут использоваться сообщения по умолчанию.")
messages_in_lang = msg_dict.get(lang, {})
# {id: {'user_bird': Bird_obj, 'npc_bird': BirdNPC_obj}}
state = {}

# {id: {'w': 0, 'l': 6, 'e': 3}, ...}
statistics = {}

stat_file = "game_stat.json"

body_part_keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True,
                                               one_time_keyboard=True,
                                               row_width=len(BodyPart))

body_part_keyboard.row(*[types.KeyboardButton(body_part.name) for body_part in BodyPart])


def update_save_stat(user_id, result: GameResult):
    print("Обновление статистики", end="...")
    global statistics

    user_id = str(user_id)

    if statistics.get(user_id, None) is None:
        statistics[user_id] = {}

    if result == GameResult.WIN:
        statistics[user_id]['W'] = statistics[user_id].get('W', 0) + 1
    elif result == GameResult.DEFEAT:
        statistics[user_id]['L'] = statistics[user_id].get('L', 0) + 1
    elif result == GameResult.DRAW:
        statistics[user_id]['E'] = statistics[user_id].get('E', 0) + 1
    else:
        print(f"Не существует результата {result}")

    with open(stat_file, 'w') as file:
        json.dump(statistics, file)

    print("завершено!")


def load_stat():
    print('Загрузка статистики...', end='')
    global statistics

    try:
        with open(stat_file, 'r') as file:
            statistics = json.load(file)
        print('завершена успешно!')
    except FileNotFoundError as my_error:
        statistics = {}
        print('файл не найден!')


@bot.message_handler(commands=["help", "info"])
def help_command(message):
    help_start_msg = messages_in_lang.get("help_start", "для начала игры")
    help_stat_msg = messages_in_lang.get("help_stat", "для отображения статистики")
    help_settings_msg = messages_in_lang.get("help_settings", "для изменения языка")
    bot.send_message(message.chat.id,
                     f"Hi!\n/start {help_start_msg}\n/stat {help_stat_msg}\n/settings {help_settings_msg}")


@bot.message_handler(commands=["stat"])
def stat(message):
    global statistics
    if statistics.get(str(message.chat.id), None) is None:
        user_stat = messages_in_lang.get("stat_no_game", "Еще не было ни одной игры")  # "Еще не было ни одной игры"
    else:
        user_stat = messages_in_lang.get("stat_your_results", "Твои результаты:")  # "Твои результаты:"
        for res, num in statistics[str(message.chat.id)].items():
            user_stat += f"\n{res}: {num}"

    bot.send_message(message.chat.id,
                     text=user_stat)


# pnikolae 07.06.2021
@bot.message_handler(commands=["settings"])
def settings(message):
    lang_keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True,
                                              one_time_keyboard=True,
                                              row_width=2)
    lang_keyboard.row(US, RU)
    bot.send_message(message.from_user.id,
                     text="Set the language",
                     reply_markup=lang_keyboard)
    bot.register_next_step_handler(message, settings_handler)


def settings_handler(message):
    #print(message.text)
    #if message.text == RU:
    #    print("вы выбрали русский")
    #elif message.text == US:
    #    print("your choice is english")
    #else:
    #    print("english by default")
    global lang
    if message.text == RU:
        lang = "ru"
    elif message.text == US:
        lang = "en"
    else:
        lang = "df"
    global messages_in_lang
    messages_in_lang = msg_dict.get(lang, {})


@bot.message_handler(commands=["start"])
def start(message):
    yes_no_keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True,
                                                one_time_keyboard=True,
                                                row_width=2)
    yes = messages_in_lang.get("yes", "Да")
    no = messages_in_lang.get("no", "Нет")
    yes_no_keyboard.row(yes, no)

    are_you_ready = messages_in_lang.get("are_you_ready", "Готов начать битву?")

    bot.send_message(message.from_user.id,
                     text=are_you_ready,
                     reply_markup=yes_no_keyboard)

    bot.register_next_step_handler(message, start_question_handler)


def start_question_handler(message):
    if message.text == messages_in_lang.get("yes", "Да"):
        reply = messages_in_lang.get("yes_reply", 'Хорошо, начинаем!')
        bot.send_message(message.from_user.id, reply)

        create_npc(message)

        ask_user_about_bird_type(message)

    elif message.text == messages_in_lang.get("no", "Нет"):
        reply = messages_in_lang.get("no_reply", 'Ок, я подожду.')
        bot.send_message(message.from_user.id, reply)
    else:
        reply = messages_in_lang.get("wrong_answer_reply", 'Я не знаю такого варианта ответа!')
        bot.send_message(message.from_user.id, reply)


def create_npc(message):
    print(f"Начало создания объекта NPC для chat id = {message.chat.id}")
    global state
    bird_npc = BirdNPC()
    if state.get(message.chat.id, None) is None:
        state[message.chat.id] = {}
    state[message.chat.id]['npc_bird'] = bird_npc

    npc_image_filename = birds_by_type[bird_npc.bird_type][bird_npc.name][0]
    enemy = messages_in_lang.get("enemy", 'Противник:')
    bot.send_message(message.chat.id, enemy)
    with open(f"../images/{npc_image_filename}", 'rb') as file:
        bot.send_photo(message.chat.id, file, bird_npc)
    print(f"Завершено создание объекта NPC для chat id = {message.chat.id}")


def ask_user_about_bird_type(message):
    markup = types.InlineKeyboardMarkup()

    # TODO: несколько кнопок в ряду
#    for bird_type in BirdType:
#        markup.add(types.InlineKeyboardButton(text=bird_type.name,
#                                              callback_data=f"bird_type_{bird_type.value}"))
    markup.add(*(types.InlineKeyboardButton(text=bird_type.name + f" ({len(birds_by_type[bird_type])})",
                                            callback_data=f"bird_type_{bird_type.value}") for bird_type in BirdType),
               row_width=ceil(sqrt(len(BirdType))))
    #markup.(2) pnikolae 07.06.2021
    invitation = messages_in_lang.get("choose_type_prompt", "Выбери тип птицы:")

    bot.send_message(message.chat.id, invitation, reply_markup=markup)


@bot.callback_query_handler(func=lambda call: "bird_type_" in call.data)
def bird_type_handler(call):
    call_data_split = call.data.split("_")
    if len(call_data_split) != 3 or not call_data_split[2].isdigit():
        trouble = messages_in_lang.get("trouble", "Возникла проблема. Перезапусти сессию!")
        bot.send_message(call.message.chat.id, trouble)
    else:
        bird_type_id = int(call_data_split[2])

        invitation = messages_in_lang.get("choose_bird_prompt", "Выбери свою птицу:")
        bot.send_message(call.message.chat.id, invitation)

        ask_user_about_bird_by_type(bird_type_id, call.message)


def ask_user_about_bird_by_type(bird_type_id, message):
    bird_type = BirdType(bird_type_id)
    bird_dict_by_type = birds_by_type.get(bird_type, {})

    # TODO: несколько кнопок в ряду
    # for bird_name, bird_img in bird_dict_by_type.items():
    #     markup = types.InlineKeyboardMarkup()
    #     markup.add(types.InlineKeyboardButton(text=bird_name,
    #                                           callback_data=f"bird_name_{bird_type_id}_{bird_name}"))
    #     with open(f"../images/{bird_img[0]}", 'rb') as file:
    #         bot.send_photo(message.chat.id, file, reply_markup=markup)
    #pnikolae
    for bird_name, bird_img in bird_dict_by_type.items():
        markup = types.InlineKeyboardMarkup()
        markup.add(types.InlineKeyboardButton(text=bird_name,
                                              callback_data=f"bird_name_{bird_type_id}_{bird_name}"))
        with open(f"../images/{bird_img[0]}", 'rb') as file:
            bot.send_photo(message.chat.id, file, reply_markup=markup)


@bot.callback_query_handler(func=lambda call: "bird_name_" in call.data)
def bird_name_handler(call):
    call_data_split = call.data.split("_")
    if len(call_data_split) != 4 or not call_data_split[2].isdigit():
        trouble = messages_in_lang.get("trouble", "Возникла проблема. Перезапусти сессию!")
        bot.send_message(call.message.chat.id, trouble)
    else:
        bird_type_id, bird_name = int(call_data_split[2]), call_data_split[3]

        create_user_bird(call.message, bird_type_id, bird_name)

        start_game_msg = messages_in_lang.get("start_game", "Игра началась!")
        bot.send_message(call.message.chat.id, start_game_msg)
        location_val = random.randint(BiotopeType.min_value(), BiotopeType.max_value())
        global location
        location = BiotopeType(location_val)
        game_loc_msg = f"{messages_in_lang.get('location', 'Место действия:')} {location.name}"
        bot.send_message(call.message.chat.id, game_loc_msg)

        game_next_step(call.message)


def create_user_bird(message, bird_type_id, bird_name):
    print(f"Начало создания объекта Bird для chat id = {message.chat.id}")
    global state
    user_bird = Bird(name=bird_name,
                     bird_type=BirdType(bird_type_id))

    if state.get(message.chat.id, None) is None:
        state[message.chat.id] = {}
    state[message.chat.id]['user_bird'] = user_bird

    image_filename = birds_by_type[user_bird.bird_type][user_bird.name][0]
    your_choice_msg = messages_in_lang.get("your_choice", 'Твой выбор:')
    bot.send_message(message.chat.id, your_choice_msg)
    with open(f"../images/{image_filename}", 'rb') as file:
        bot.send_photo(message.chat.id, file, user_bird)

    print(f"Завершено создание объекта Bird для chat id = {message.chat.id}")


def game_next_step(message: Message):
    defence_msg = messages_in_lang.get("select_defence", "Выбор точки для защиты:")
    bot.send_message(message.chat.id,
                     defence_msg,
                     reply_markup=body_part_keyboard)

    bot.register_next_step_handler(message, reply_defend)


def reply_defend(message: Message):
    if not BodyPart.has_item(message.text):
        wrong_input_msg = messages_in_lang.get("wrong_input", "Необходимо выбрать вариант на клавиатуре!")
        bot.send_message(message.chat.id, wrong_input_msg)
        game_next_step(message)
    else:
        attack_msg = messages_in_lang.get("select_attack", "Выбор точки для удара:")
        bot.send_message(message.chat.id,
                         attack_msg,
                         reply_markup=body_part_keyboard)

        bot.register_next_step_handler(message, reply_attack, defend_body_part=message.text)


def reply_attack(message: Message, defend_body_part: str):
    if not BodyPart.has_item(message.text):
        wrong_input_msg = messages_in_lang.get("wrong_input", "Необходимо выбрать вариант на клавиатуре!")
        bot.send_message(message.chat.id, wrong_input_msg)
        game_next_step(message)
    else:
        attack_body_part = message.text

        global state
        # обращение по ключу ко вложенному словарю для получения объекта покемона пользователя
        user_bird = state[message.chat.id]['user_bird']

        # обращение по ключу ко вложенному словарю для получения объекта покемона npc
        bird_npc = state[message.chat.id]['npc_bird']

        # конвертировать строку хранящуюся в объекте attack_body_part в правильный тип enum BodyPart
        # конвертировать строку хранящуюся в объекте defend_body_part в правильный тип enum BodyPart
        user_bird.next_step_points(next_attack=BodyPart[attack_body_part],
                                   next_defence=BodyPart[defend_body_part])

        bird_npc.next_step_points()

        game_step(message, user_bird, bird_npc)


def game_step(message: Message, user_bird: Bird, bird_npc: BirdNPC):
    comment_npc = bird_npc.get_hit(opponent_attack_point=user_bird.attack_point,
                                   opponent_hit_power=user_bird.hit_power,
                                   opponent_type=user_bird.bird_type,
                                   game_location=location)
    comment_npc_msg = messages_in_lang.get(comment_npc, comment_npc)
    bot.send_message(message.chat.id, f"NPC bird: {comment_npc_msg}\nHP: {bird_npc.hp}")

    comment_user = user_bird.get_hit(opponent_attack_point=bird_npc.attack_point,
                                     opponent_hit_power=bird_npc.hit_power,
                                     opponent_type=bird_npc.bird_type,
                                     game_location=location)
    comment_user_msg = messages_in_lang.get(comment_user, comment_user)
    bot.send_message(message.chat.id, f"Your bird: {comment_user_msg}\nHP: {user_bird.hp}")

    if bird_npc.state == PlayerState.READY and user_bird.state == PlayerState.READY:
        show_must_go_on_msg = messages_in_lang.get("show_must_go_on", "Продолжаем!")
        bot.send_message(message.chat.id, show_must_go_on_msg)
        game_next_step(message)
    elif bird_npc.state == PlayerState.DEFEATED and user_bird.state == PlayerState.DEFEATED:
        draw_msg = messages_in_lang.get("draw", "Ничья!")
        bot.send_message(message.chat.id, draw_msg)
        update_save_stat(message.chat.id, GameResult.DRAW)
    elif bird_npc.state == PlayerState.DEFEATED:
        you_win_msg = messages_in_lang.get("you_win", "Ты победил :)")
        bot.send_message(message.chat.id, you_win_msg)
        song_filename = birds_by_type[user_bird.bird_type][user_bird.name][-1]
        try:
            with open(f"../songs/{song_filename}", 'rb') as file:
                #bot.send_audio(message.chat.id, file)  # pnikolae 07.06.2021
                bot.send_voice(message.chat.id, file)
        except FileNotFoundError as my_error:
            print(f"файл {song_filename} не найден")
        update_save_stat(message.chat.id, GameResult.WIN)
    elif user_bird.state == PlayerState.DEFEATED:
        you_lose_msg = messages_in_lang.get("you_lose", "Ты проиграл :)")
        bot.send_message(message.chat.id, you_lose_msg)
        update_save_stat(message.chat.id, GameResult.DEFEAT)


if __name__ == '__main__':
    load_stat()

    print('Starting the bot...')
    bot.polling(none_stop=True, interval=0)
    print('The bot has stopped!')
