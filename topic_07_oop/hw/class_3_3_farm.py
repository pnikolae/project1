"""
Класс Farm.

Поля:
животные (list из произвольного количества Goat и Chicken): zoo_animals
(вначале список пуст, потом добавялем животных методами append и extend самостоятельно),
наименование фермы: name,
имя владельца фермы: owner.

Методы:
get_goat_count: вернуть количество коз на ферме (*Подсказка isinstance или type == Goat),
get_chicken_count: вернуть количество куриц на ферме,
get_animals_count: вернуть количество животных на ферме,
get_milk_count: вернуть сколько молока можно получить в день,
get_eggs_count: вернуть сколько яиц можно получить в день.
"""
from topic_07_oop.hw.class_3_1_chicken import Chicken
from topic_07_oop.hw.class_3_2_goat import Goat


class Farm:
    def __init__(self, nm, ownr):
        self.name = nm
        self.owner = ownr
        self.zoo_animals = []

    def get_goat_count(self):
        type_of_animals = [type(x) for x in self.zoo_animals]
        return type_of_animals.count(Goat)

    def get_chicken_count(self):
        chickens = [animal for animal in self.zoo_animals if isinstance(animal, Chicken)]
        return len(chickens)

    def get_animals_count(self):
        return len(self.zoo_animals)

    def get_milk_count(self):
        total_milk_per_day = 0
        for animal in self.zoo_animals:
            if type(animal) == Goat:
                total_milk_per_day += animal.milk_per_day
        return total_milk_per_day

    def get_eggs_count(self):
        total_eggs_per_day = 0
        for animal in self.zoo_animals:
            if type(animal) == Chicken:
                total_eggs_per_day += animal.eggs_per_day
        return total_eggs_per_day


if __name__ == '__main__':

    f = Farm("Ranch", "John Doe")
    print(f.get_goat_count())
    print(f.get_chicken_count())
    print(f.get_eggs_count())
    print(f.get_milk_count())
    print(f.get_animals_count())
    print("-" * 20)

    f.zoo_animals.append(Chicken("Ряба", 100, 1))
    print(f.get_goat_count())
    print(f.get_chicken_count())
    print(f.get_eggs_count())
    print(f.get_milk_count())
    print(f.get_animals_count())
    print("-" * 20)

    f.zoo_animals.append(Chicken("Пеструшка", 100, 2))
    print(f.get_goat_count())
    print(f.get_chicken_count())
    print(f.get_eggs_count())
    print(f.get_milk_count())
    print(f.get_animals_count())
    print("-"*20)

    f.zoo_animals.extend([Goat("Дереза", 2, 3), Goat("Маруся",3, 5)])
    print(f.get_goat_count())
    print(f.get_chicken_count())
    print(f.get_eggs_count())
    print(f.get_milk_count())
    print(f.get_animals_count())