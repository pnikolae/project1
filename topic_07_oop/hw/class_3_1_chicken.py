"""
Класс Chicken.

Поля:
имя: name,
номер загона: corral_num,
сколько яиц в день: eggs_per_day.

Методы:
get_sound: вернуть строку 'Ко-ко-ко',
get_info: вернуть строку вида:
"Имя курицы: name
 Номер загона: corral_num
 Количество яиц в день: eggs_per_day"
__lt__: вернуть результат сравнения количества яиц (<)
"""


class Chicken:
    def __init__(self, nm, corral_num_, eggs_per_day_):
        self.name = nm
        self.corral_num = corral_num_
        self.eggs_per_day = eggs_per_day_

    def get_sound(self):
        return "Ко-ко-ко"

    def get_info(self):
        return f"Имя курицы: {self.name}\nНомер загона: {self.corral_num}\nКоличество яиц в день: {self.eggs_per_day}"

    def __lt__(self, other):
        return self.eggs_per_day < other.eggs_per_day


if __name__ == '__main__':
    r = Chicken("Ряба", 1, 1)
    b = Chicken("Чернушка", 2, 2)
    p = Chicken("Пеструшка", 2, 0.5)

    print(r.get_info())
    print(b.get_info())
    print(p.get_info())
    # print(Chicken.get_sound())
    print(b.get_sound())

    if r < b:
        print("Чернушка несет больше Рябы")
    else:
        print("Ряба несет неменьше Чернушки")
