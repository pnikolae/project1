"""
Класс School.

Поля:
список людей в школе (общий list для Pupil и Worker): people,
номер школы: number.

Методы:
get_avg_mark: вернуть средний балл всех учеников школы
get_avg_salary: вернуть среднюю зп работников школы
get_worker_count: вернуть сколько всего работников в школе
get_pupil_count: вернуть сколько всего учеников в школе
get_pupil_names: вернуть все имена учеников (с повторами, если есть)
get_unique_worker_positions: вернуть список всех должностей работников (без повторов, подсказка: set)
get_max_pupil_age: вернуть возраст самого старшего ученика
get_min_worker_salary: вернуть самую маленькую зп работника
get_min_salary_worker_names: вернуть список имен работников с самой маленькой зп
(список из одного или нескольких элементов)
"""
from topic_07_oop.hw.class_4_1_pupil import Pupil
from topic_07_oop.hw.class_4_2_worker import Worker
import numpy as np

class School:
    def __init__(self, pple, numb):
        self.people = pple
        self.number = numb

        self.__workers__ = [x for x in self.people if type(x) == Worker]
        self.__pupils__ = [x for x in self.people if type(x) == Pupil]

    def get_avg_mark(self):
        avg_marks = [x.get_avg_mark() for x in self.__pupils__]
        return np.average(avg_marks)

    def get_avg_salary(self):
        return np.average([x.salary for x in self.__workers__])

    def get_worker_count(self):
        return len(self.__workers__)

    def get_pupil_count(self):
        return len(self.__pupils__)

    def get_pupil_names(self):
        return [x.name for x in self.__pupils__]

    def get_unique_worker_positions(self):
        return set([x.position for x in self.__workers__])

    def get_max_pupil_age(self):
        return np.max([x.age for x in self.__pupils__])

    def get_min_worker_salary(self):
        return np.min([x.salary for x in self.__workers__])

    def get_min_salary_worker_names(self):
        return [x.name for x in self.__workers__ if x.salary == self.get_min_worker_salary()]


if __name__ == '__main__':
    p1 = Pupil("Петр", 12, {"math": [3, 4, 4], "history": [4, 5, 3]})
    p2 = Pupil("Иван", 10, {"math": [5, 3]})

    t1 = Worker("Эмма Петровна", 1000, "Директор")
    t2 = Worker("Алла Ивановна", 800, "Завуч")

    s = School([p1, t1, p2, t2], 101)

    print("get_avg_mark: ", s.get_avg_mark())
    print("get_avg_salary: ", s.get_avg_salary())
    print("get_worker_count: ", s.get_worker_count())
    print("get_pupil_count: ", s.get_pupil_count())
    print("get_pupil_names: ", s.get_pupil_names())
    print("get_unique_worker_positions: ", s.get_unique_worker_positions())
    print("get_max_pupil_age: ", s.get_max_pupil_age())
    print("get_min_worker_salary: ", s.get_min_worker_salary())
    print("get_min_salary_worker_names: ", s.get_min_salary_worker_names())
    #help(np.max)
