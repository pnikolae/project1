"""
Класс Goat.

Поля:
имя: name,
возраст: age,
сколько молока дает в день: milk_per_day.

Методы:
get_sound: вернуть строку 'Бе-бе-бе',
__invert__: реверс строки с именем (например, была Маруся, а стала Ясурам). вернуть self
__mul__: умножить milk_per_day на число. вернуть self
"""


class Goat:
    def __init__(self, nm, age_, milk_per_day_):
        self.name = nm
        self.age = age_
        self.milk_per_day = milk_per_day_

    def get_sound(self):
        return "Бе-бе-бе"

    def __invert__(self):
        self.name = self.name[::-1].title()
        return self

    def __mul__(self, other):
        self.milk_per_day *= other
        return self

    def __rmul__(self, other):
        return self * other


if __name__ == '__main__':
    goat1 = Goat("Маруся", 1, 3)
    goat2 = Goat("Дереза", 4, 1)

    print((goat1 * 2).milk_per_day)
    print((2 * goat1).milk_per_day)

    print(goat1.get_sound())
    print((~goat2).name)
