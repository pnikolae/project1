"""
Класс Human.

Поля:
age,
first_name,
last_name.

При создании экземпляра инициализировать поля класса.

Создать метод get_age, который возвращает возраст человека.

Перегрузить оператор __eq__, который сравнивает объект человека с другим по атрибутам.

Перегрузить оператор __str__, который возвращает строку в виде "Имя: first_name last_name Возраст: age".
"""


class Human:

    def __init__(self, age_=27, first_name_="John", last_name_="Doe"):
        self.age = age_
        self.first_name = first_name_
        self.last_name = last_name_

    def get_age(self):
        return self.age

    def __eq__(self, other):
        return self.age == other.age and self.first_name == other.first_name and self.last_name == other.last_name

    def __str__(self):
        return f"Имя: {self.first_name} {self.last_name} Возраст: {self.age}"


if __name__ == '__main__':
    mann = Human()
    print(mann)
    johan = Human(20, "Johan", "de Boer")
    print(johan)
    if johan == mann:
        print("Так вы тезки, ребята!")
    else:
        print("Чего и следовало ожидать...")
    if mann.get_age() > johan.get_age():
        print("Джон старше Йохана.")
    else:
        print("Йохан не моложе Джона.")

