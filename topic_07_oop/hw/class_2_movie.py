"""
Класс Movie.

Поля:
duration_min,
name,
year.

При создании экземпляра инициализировать поля класса.

Перегрузить оператор __str__, который возвращает строку вида
"Наименование фильма: name | Год выпуска: year | Длительность (мин): duration_min".

Перегрузить оператор __ge__, который
возвращает True, если self.duration_min => other_duration_min, иначе False.
"""


class Movie:
    def __init__(self, dur_min, nm, y):
        self.duration_min = dur_min
        self.name = nm
        self.year = y

    def __str__(self):
        return f"Наименование фильма: {self.name} | Год выпуска: {self.year} | Длительность (мин): {self.duration_min}"

    def __ge__(self, other):
        return self.duration_min >= other.duration_min


if __name__ == '__main__':
    mov = Movie(116, "Back To the Future", 1985)
    mov2 = Movie(129, "Анискин и Фантомас", 1973)
    print(mov)
    print(mov2)
    if mov >= mov2:
        print("Американский фильм не короче советского")
    else:
        print("Советский фильм длиннее американского")
