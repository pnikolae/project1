"""
Класс Pupil.

Поля:
имя: name,
возраст: age,
dict с оценками: marks (пример: {'math': [3, 5], 'english': [5, 5, 4] ...].

Методы:
get_all_marks: получить список всех оценок,
get_avg_mark_by_subject: получить средний балл по предмету (если предмета не существует, то вернуть 0),
get_avg_mark: получить средний балл (все предметы),
__le__: вернуть результат сравнения (<=) средних баллов (все предметы) двух учеников.
"""
import numpy as np

class Pupil:
    def __init__(self, nm, age_, marks_: dict):
        self.name = nm
        self.age = age_
        self.marks = marks_

    def get_all_marks(self):
        all_marks = []
        for val in self.marks.values():
            all_marks.extend(val)
        return all_marks

    def get_avg_mark_by_subject(self, subject):
        return np.average(self.marks.get(subject, [0]))

    def get_avg_mark(self):
        return np.average(self.get_all_marks())

    def __le__(self, other):
        return self.get_avg_mark() <= other.get_avg_mark()


if __name__ == "__main__":
    p = Pupil("Петр", 12, {"math": [3, 4, 4], "history": [4, 5, 3]})
    p1 = Pupil("Иван", 10, {"math": [5, 3]})
    print(p.get_all_marks())
    print(p.get_avg_mark())
    print(p1.get_avg_mark_by_subject("math"))
    print(p1.get_avg_mark_by_subject("history"))
    if p1 <= p:
        print(f"{p.name} учится нехуже, чем {p1.name}")
    else:
        print(f"{p1.name} учится лучше, чем {p.name}")