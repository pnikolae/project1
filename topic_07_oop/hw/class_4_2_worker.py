"""
Класс Worker.

Поля:
имя: name,
зарплата: salary,
должность: position.

Методы:
__gt__: возвращает результат сравнения (>) зарплат работников.
__len__: возвращает количетсво букв в названии должности.
"""


class Worker:
    def __init__(self, nm, sal, pos):
        self.name = nm
        self.salary = sal
        self.position = pos

    def __gt__(self, other):
        return self.salary > other.salary

    def __len__(self):
        return len(self.position)

if __name__ == '__main__':
    t1 = Worker("Эмма Петровна", 1000, "Директор")
    t2 = Worker("Алла Ивановна", 800, "Завуч")
    print(len(t1))
    print(t1 > t2)