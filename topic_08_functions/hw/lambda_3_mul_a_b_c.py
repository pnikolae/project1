"""
lambda перемножает аргументы a, b и c и выводит результат
"""

d = lambda a, b, c: print(a * b * c)

if __name__ == '__main__':
    d(-1, 2, 3)
