"""
Функция print_soap_price.

Принимает 2 аргумента: название мыла (строка) и неопределенное количество цен (*args).

Функция print_soap_price выводит вначале название мыла, а потом все цены на него.

В функции main вызывается функция print_soap_price и передается название мяла и произвольное количество цен.

Пример: print_soap_price('Dove’, 10, 50) или print_soap_price('Мылко’, 456, 876, 555).
"""


def print_soap_price(soap_name, *args):
    if type(soap_name) != str:
        print("The first argument must be a string!")
        return

    print(f"{soap_name}: {', '.join([str(x) for x in args])}")


if __name__ == '__main__':
    print_soap_price('Dove', 10, 50)
    print_soap_price('Мылко', 456, 876, 555)
    print_soap_price('Детское')


