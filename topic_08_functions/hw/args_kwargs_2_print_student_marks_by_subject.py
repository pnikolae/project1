"""
Функция print_student_marks_by_subject.

Принимает 2 аргумента: имя студента и именованные аргументы с оценками по различным предметам (**kwargs).

Выводит на экран имя студента, а затем предмет и оценку (может быть несколько, если курс состоял из нескольких частей).

Пример вызова функции print_student_marks_by_subject("Вася", math=5, biology=(3, 4), magic=(4, 5, 5)).
"""


def print_student_marks_by_subject(student_nm, **kwargs):
    if type(student_nm) != str:
        print("The first argument must be a string.")
        return

    print(student_nm + ":")
    for subject, marks in kwargs.items():
        if int == type(marks) or str == type(marks):
            print(f"{subject}: {marks}")
        else:
            print(f"{subject}: {', '.join([str(x) for x in marks])}")


if __name__ == '__main__':
    print_student_marks_by_subject("Вася", math="отлично", english=("отлично", "хорошо"),
                                   chemistry=5, geography=[3, 4], biology=(3, 4), magic=(4, 5, 5))
    print_student_marks_by_subject("Петров")
