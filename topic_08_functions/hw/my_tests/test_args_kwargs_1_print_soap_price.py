from topic_08_functions.hw.args_kwargs_1_print_soap_price import *

soap_price = "Soap"


def test_print_soap_price_wrong_arg_type(capsys):
    print_soap_price(1, 2)
    out, err = capsys.readouterr()
    assert out == "The first argument must be a string!" + "\n"


def test_print_soap_price_0args(capsys):
    print_soap_price(soap_price)
    out, err = capsys.readouterr()
    assert out == soap_price + ": \n"


def test_print_soap_price_1arg(capsys):
    print_soap_price(soap_price, 1)
    out, err = capsys.readouterr()
    assert out == soap_price + ": " + "1\n"


def test_print_soap_price_2args(capsys):
    print_soap_price(soap_price, 1, 2)
    out, err = capsys.readouterr()
    assert out == soap_price + ": " + "1, 2\n"


def test_print_soap_price_3args(capsys):
    print_soap_price(soap_price, 1, 2, 3)
    out, err = capsys.readouterr()
    assert out == soap_price + ": " + "1, 2, 3\n"


def test_print_soap_price_float_args(capsys):
    print_soap_price(soap_price, 10.50, 2.0, 3.25)
    out, err = capsys.readouterr()
    assert out == soap_price + ": " + "10.5, 2.0, 3.25\n"
