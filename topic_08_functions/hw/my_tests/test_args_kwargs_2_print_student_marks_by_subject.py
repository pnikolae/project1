from topic_08_functions.hw.args_kwargs_2_print_student_marks_by_subject import *


def test_print_student_marks_by_subject_wrong_arg_type(capsys):
    print_student_marks_by_subject(1)
    out, err = capsys.readouterr()
    assert out == "The first argument must be a string." + "\n"


def test_print_student_marks_by_subject_0subjects(capsys):
    print_student_marks_by_subject("Вася")
    out, err = capsys.readouterr()
    assert out == "Вася:" + "\n"


def test_print_student_marks_by_subject_1subj_1mark(capsys):
    print_student_marks_by_subject("Вася", math=5)
    out, err = capsys.readouterr()
    assert out == "Вася:" + "\n" + "math: " + "5\n"


def test_print_student_marks_by_subject_1subj_1mark_as_list(capsys):
    print_student_marks_by_subject("Вася", math=[5])
    out, err = capsys.readouterr()
    assert out == "Вася:" + "\n" + "math: " + "5\n"


def test_print_student_marks_by_subject_1subj_tuple_of_marks(capsys):
    print_student_marks_by_subject("Вася", math=(5, 4))
    out, err = capsys.readouterr()
    assert out == "Вася:" + "\n" + "math: " + "5, 4\n"


def test_print_student_marks_by_subject_1subj_list_of_marks(capsys):
    print_student_marks_by_subject("Вася", math=[5, 4])
    out, err = capsys.readouterr()
    assert out == "Вася:" + "\n" + "math: " + "5, 4\n"


def test_print_student_marks_by_subject_2subj_list_of_marks(capsys):
    print_student_marks_by_subject("Вася", math=[5, 4], english=[3, 4, 4])
    out, err = capsys.readouterr()
    assert out == "Вася:" + "\n" + "math: " + "5, 4\n" + "english: 3, 4, 4\n"


def test_print_student_marks_by_subject_1subj_mark_as_string(capsys):
    print_student_marks_by_subject("Вася", math="отлично")
    out, err = capsys.readouterr()
    assert out == "Вася:" + "\n" + "math: " + "отлично\n"


def test_print_student_marks_by_subject_1subj_marks_as_string(capsys):
    print_student_marks_by_subject("Вася", math=("отлично", "отлично"))
    out, err = capsys.readouterr()
    assert out == "Вася:" + "\n" + "math: " + "отлично, отлично\n"


def test_print_student_marks_by_subject_mixed(capsys):
    print_student_marks_by_subject("Вася", math=("отлично", "отлично"),
                                   english=4, chemistry=(3, 4), geography="удовлетворительно",
                                   history=[5, 4])
    out, err = capsys.readouterr()
    ref = "Вася:" + "\n" + "math: " + "отлично, отлично\n"\
    "english: 4\n" + "chemistry: 3, 4\n" + "geography: удовлетворительно\n"\
    "history: 5, 4\n"
    assert out == ref
