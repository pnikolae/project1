from topic_08_functions.hw.default_args_flower_with_vals import *


def test_flower_with_default_vals_wrong_flower():
    res = flower_with_default_vals(flower=1, price=10, colour="красный")
    assert res == "Flower and colour must be strings!"


def test_flower_with_default_vals_wrong_colour():
    res = flower_with_default_vals(flower="роза", price=10, colour=25.3)
    assert res == "Flower and colour must be strings!"


def test_flower_with_default_vals_wrong_price1():
    res = flower_with_default_vals(flower="роза", colour="красный", price=0)
    assert res == "Price should be positive, but less then 10 000."


def test_flower_with_default_vals_wrong_price2():
    res = flower_with_default_vals(flower="роза", colour="красный", price=-1)
    assert res == "Price should be positive, but less then 10 000."


def test_flower_with_default_vals_wrong_price3():
    res = flower_with_default_vals(flower="роза", colour="красный", price=10000)
    assert res == "Price should be positive, but less then 10 000."


def test_flower_with_default_vals_wrong_price4():
    res = flower_with_default_vals(flower="роза", colour="красный", price=10001)
    assert res == "Price should be positive, but less then 10 000."


def test_flower_with_default_vals_given_args(capsys):
    flower_with_default_vals(flower="роза", colour="красный", price=25.0)
    out, err = capsys.readouterr()
    assert out == "Цветок: роза | Цвет: красный | Цена: 25.0\n"


def test_flower_with_default_vals_default_all_args(capsys):
    flower_with_default_vals()
    out, err = capsys.readouterr()
    assert out == "Цветок: ромашка | Цвет: белый | Цена: 10.25\n"


def test_flower_with_default_vals_default_except_flower(capsys):
    flower_with_default_vals(flower="роза")
    out, err = capsys.readouterr()
    assert out == "Цветок: роза | Цвет: белый | Цена: 10.25\n"


def test_flower_with_default_vals_default_except_colour(capsys):
    flower_with_default_vals(colour="красный")
    out, err = capsys.readouterr()
    assert out == "Цветок: ромашка | Цвет: красный | Цена: 10.25\n"


def test_flower_with_default_vals_default_except_price(capsys):
    flower_with_default_vals(price=25.0)
    out, err = capsys.readouterr()
    assert out == "Цветок: ромашка | Цвет: белый | Цена: 25.0\n"


def test_flower_with_default_vals_default_price(capsys):
    flower_with_default_vals(colour="красный", flower="роза")
    out, err = capsys.readouterr()
    assert out == "Цветок: роза | Цвет: красный | Цена: 10.25\n"


def test_flower_with_default_vals_default_colour(capsys):
    flower_with_default_vals(flower="роза", price=25.0)
    out, err = capsys.readouterr()
    assert out == "Цветок: роза | Цвет: белый | Цена: 25.0\n"


def test_flower_with_default_vals_default_flower(capsys):
    flower_with_default_vals(colour="красный", price=25.0)
    out, err = capsys.readouterr()
    assert out == "Цветок: ромашка | Цвет: красный | Цена: 25.0\n"
