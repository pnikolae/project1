"""
Функция check_substr.

Принимает две строки.
Если меньшая по длине строка содержится в большей, то возвращает True,
иначе False.
Если строки равны, то False.
Если одна из строк пустая, то True.
"""


def check_substr(str1, str2):
    if len(str1) < len(str2):
        return str1 in str2
    elif len(str2) < len(str1):
        return str2 in str1
    else:
        return False


if __name__ == '__main__':
    print('1:', check_substr('', ''))
    print('2:', check_substr('123', '23'))
    print('3:', check_substr('23', '123'))
    print('4:', check_substr('123', '123'))
    print('5:', check_substr('123', ''))
    print('6:', check_substr('', '123'))
    print('7:', check_substr('3', '12'))
    print('8:', check_substr('12', '3'))
