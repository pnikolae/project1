"""
Функция sum_1_112_3.

Вернуть сумму 1+4+7+10+...109+112.
"""


def sum_1_112_3():
    sum = 0
    for i in range(1, 113, 3):
        sum += i
    return sum


if __name__ == '__main__':
    print(sum_1_112_3())
