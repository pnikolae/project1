"""
Функция if_3_do.

Принимает число.
Если оно больше 3, то увеличить число на 10, иначе уменьшить на 10.
Вернуть результат.
"""


def if_3_do(num):
    if num > 3:
        num += 10
    else:
        num -= 10
    return num


if __name__ == '__main__':
    print(if_3_do(2))
    print(if_3_do(3))
    print(if_3_do(4))

    print(if_3_do(0.5))
    print(if_3_do(3.0001))

# check how function params passed in Python
    n = 10
    if_3_do(n)
    print(f'n == {n}')