"""
Функция count_odd_num.

Принимает натуральное число (целое число > 0).
Верните количество нечетных цифр в этом числе.
Если число меньше или равно 0, то вернуть "Must be > 0!".
Если число не целое (не int, а другой тип данных), то вернуть "Must be int!".
"""


def count_odd_num(num):
    odd_counter = 0
    if type(num) == int:
        if num <= 0:
            return "Must be > 0!"
        else:
            for digit in str(num):
                odd_counter = odd_counter if int(digit) % 2 == 0 else odd_counter + 1
            return odd_counter
    else:
        return "Must be int!"


if __name__ == '__main__':
    print(count_odd_num(-51))
    print(count_odd_num(51))
    print(count_odd_num(246))
    print(count_odd_num('abc'))
    print(count_odd_num(0))

