"""
Функция check_sum.

Принимает 3 числа.
Вернуть True, если можно взять какие-то два из них и в сумме получить третье, иначе False
"""


def check_sum(a, b, c):
    if a + b == c or a + c == b or b + c == a:
        return True
    else:
        return False


if __name__ == '__main__':
    if check_sum(0, 0, 0):
        print('Ta-dam!')
    if check_sum(1, 2, 3):
        print('Ta-dam 3!')
    if check_sum(3, 2, 1):
        print('Ta-dam 1!')
    if check_sum(2, 3, 1):
        print('Ta-dam 2!')
    if not check_sum(0, 1, 0):
        print('false return checked')