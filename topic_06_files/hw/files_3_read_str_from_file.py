"""
Функция read_str_from_file.

Принимает 1 аргумент: строка (название файла или полный путь к файлу).

Выводит содержимое файла (в консоль). (* Файл должен быть предварительно создан и наполнен каким-то текстом.)
"""


def read_str_from_file(path):
    if type(path) != str:
        return "Path must be a string!"
    with open(path, "r") as f:
        print(f.read())


if __name__ == '__main__':
    read_str_from_file("my_dict_pnikolaev.txt")
