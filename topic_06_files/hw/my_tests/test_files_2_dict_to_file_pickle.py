from topic_06_files.hw.files_2_dict_to_file_pickle import *
import os.path

# проверить неправильные аргументы
# записать новый файл, удостоверившись, что его не было до этого
# проверить, что файл перезаписывается, а не добавляется в конец
# проверить содержимое файла

test_dict = {1: "one", 2: "two"}
test_path = ''


def test_save_dict_to_file_pickle_wrong_path():
    assert save_dict_to_file_pickle([1], {1: "one"}) == "Path must be a string!"


def test_save_dict_to_file_pickle_wrong_dict():
    assert save_dict_to_file_pickle("test_save_dict_to_file_classic_wrong_dict",
                                     [1]) == "Second argument must be a dict!"


def test_save_dict_to_file_pickle_exists():
    file_name = "test_save_dict_to_file_pickle"
    file_ext = ".pkl"
    new_path = file_name + file_ext
    i = 1
    while 1 == 1:
        if not os.path.exists(new_path):
            save_dict_to_file_pickle(new_path, test_dict)
            break
        else:
            new_path = file_name + "_" + str(i) + file_ext
            i += 1

    global test_path
    test_path = new_path
    assert os.path.exists(new_path)


def test_save_dict_to_file_pickle_check_content():
    global test_path
    with open(test_path, "rb") as test_file:
        result = pickle.load(test_file)
    assert result == test_dict


def test_save_dict_to_file_pickle_check_rewrite():
    global test_path
    test_dict2 = {3: "three", 4: "four"}
    save_dict_to_file_pickle(test_path, test_dict2)
    with open(test_path, "rb") as test_file:
        result = pickle.load(test_file)
    assert result == test_dict2

# def test_save_dict_to_file_classic():
