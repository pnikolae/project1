from topic_06_files.hw.files_1_save_dict_to_file_classic import *
from topic_06_files.hw.files_3_read_str_from_file import *
import os.path

# проверить неправильные аргументы
# записать новый файл, удостоверившись, что его не было до этого
# проверить, что файл перезаписывается, а не добавляется в конец
# проверить содержимое файла
# проверить файл на пустоту getsize(filename)
# isdir(s) - если ссылается на сущ. директорию, isfile(path), islink(path), realpath()

test_dict = {1: "one", 2: "two"}
test_dict2 = {3: "three", 4: "four"}
test_path = ''


def test_save_dict_to_file_classic_wrong_path():
    assert save_dict_to_file_classic([1], {1: "one"}) == "Path must be a string!"


def test_save_dict_to_file_classic_wrong_dict():
    assert save_dict_to_file_classic("test_save_dict_to_file_classic_wrong_dict",
                                     [1]) == "Second argument must be dict!"


def test_save_dict_to_file_classic_exists():
    file_name = "test_save_dict_to_file_classic_exists"
    file_ext = ".txt"
    new_path = file_name + file_ext
    i = 1
    while 1 == 1:
        if not os.path.exists(new_path):
            save_dict_to_file_classic(new_path, test_dict)
            break
        else:
            new_path = file_name + "_" + str(i) + file_ext
            i += 1

    global test_path
    test_path = new_path
    assert os.path.exists(new_path)


def test_save_dict_to_file_classic_check_content():
    with open(test_path, "r") as test_file:
        result = test_file.read()
    assert result == str(test_dict)


def test_save_dict_to_file_classic_check_rewrite():
    save_dict_to_file_classic(test_path, test_dict2)
    with open(test_path, "r") as test_file:
        result = test_file.read()
    assert result == str(test_dict2)


def test_read_str_from_file_wrong_path():
    assert read_str_from_file(1) == "Path must be a string!"


def test_read_str_from_file_ok(capsys):
    read_str_from_file(test_path)
    out, err = capsys.readouterr()
    assert out == str(test_dict2) + '\n'

