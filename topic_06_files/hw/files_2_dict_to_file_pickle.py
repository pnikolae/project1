"""
Функция save_dict_to_file_pickle.

Принимает 2 аргумента: строка (название файла или полный путь к файлу), словарь (для сохранения).

Сохраняет список в файл. Загрузить словарь и проверить его на корректность.
"""
import pickle


def save_dict_to_file_pickle(path, my_dict):
    if type(path) != str:
        return "Path must be a string!"
    if type(my_dict) != dict:
        return "Second argument must be a dict!"

    with open(path, "wb") as my_file:
        pickle.dump(my_dict, my_file)


if __name__ == '__main__':
    file = "my_dict_pnikolaev.pkl"
    my_dict = {1: "one", 2: "two", 3: "three"}
    save_dict_to_file_pickle(file, my_dict)

    with open(file, "rb") as f:
        result = pickle.load(f)

    print(f"type: {type(result)}")
    print(f"equal: {my_dict == result}")
