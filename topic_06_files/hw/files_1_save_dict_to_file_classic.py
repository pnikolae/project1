"""
Функция save_dict_to_file_classic.

Принимает 2 аргумента: строка (название файла или полный путь к файлу), словарь (для сохранения).

Сохраняет список в файл. Проверить, что записалось в файл.
"""


def save_dict_to_file_classic(path, my_dict):
    if type(path) != str:
        return "Path must be a string!"
    if type(my_dict) != dict:
        return "Second argument must be dict!"

    with open(path, 'w') as f:
        f.write(str(my_dict))


if __name__ == '__main__':
    my_path = "my_dict_pnikolaev.txt"
    my_dict = {1: "one", 2: "two", 3: "three"}
    save_dict_to_file_classic(my_path, my_dict)

    with open(my_path, "r") as my_file:
        my_dict_str = my_file.read()
        print(my_dict_str)
