"""
Функция dict_to_list.

Принимает 1 аргумент: словарь.

Возвращает список: [
список ключей,
список значений,
количество уникальных элементов в списке ключей,
количество уникальных элементов в списке значений
].

Если вместо словаря передано что-то другое, то возвращать строку 'Must be dict!'.

Если dict пуст, то возвращать ([], [], 0, 0).
"""


def dict_to_list(my_dict: dict):
    if type(my_dict) != dict:
        return 'Must be dict!'
    key_list = []; val_list = []
    for key, value in my_dict.items():
        key_list.append(key)
        val_list.append(value)
    return key_list, val_list, len(key_list), len(set(val_list))

if __name__ == '__main__':
    print(dict_to_list([1]))
    print(dict_to_list({}))
    print(dict_to_list({1: "one"}))
