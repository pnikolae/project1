"""
Функция magic_reversal.

Принимает 1 аргумент: список my_list.

Создает новый список new_list (копия my_list), порядок которого обратный my_list.

Возвращает список, который состоит из
[второго элемента (индекс=1) new_list]
+ [предпоследнего элемента new_list]
+ [весь new_list].

Пример:
входной список [1,  'aa', 99]
new_list [99, 'aa', 1]
результат ['aa', 'aa', 99, 'aa', 1].

Если список состоит из одного элемента, то "предпоследний" = единственный и "второй элемент" = единственный.

Если вместо списка передано что-то другое, то возвращать строку 'Must be list!'.
Если список пуст, то возвращать строку 'Empty list!'.

ВНИМАНИЕ: Изначальный список не должен быть изменен!
"""


def magic_reversal(my_list: list):
    if type(my_list) != list:
        return 'Must be list!'
    l = len(my_list)
    new_list = []
    if not l:
        return 'Empty list!'
    if 1 == l:
        new_list.extend(my_list * 3)
    else:
        rev_list = my_list[:]
        rev_list.reverse()

        new_list = [rev_list[1]]
        new_list.append(rev_list[-2])
        new_list.extend(rev_list)

    return new_list


if __name__ == '__main__':
    print(magic_reversal(1))
    print(magic_reversal([]))
    print(magic_reversal([1,  'aa', 99]))
    print(magic_reversal([1, 2]))
    print(magic_reversal([1]))
