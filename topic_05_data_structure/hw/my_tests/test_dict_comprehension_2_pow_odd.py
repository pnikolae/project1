from topic_05_data_structure.hw.dict_comprehension_2_pow_odd import *


def test_pow_odd_wrong_n():
    assert pow_odd("a") == 'Must be int!'
    assert pow_odd([1, 2]) == 'Must be int!'


def test_pow_odd_empty():
    assert pow_odd(0) == {}


def test_pow_odd_bv():
    assert pow_odd(1) == {1: 1}


def test_pow_odd_ok():
    assert pow_odd(3) == {1: 1, 3: 9}


def test_pow_odd_ok2():
    assert pow_odd(5) == {1: 1, 3: 9, 5: 25}
