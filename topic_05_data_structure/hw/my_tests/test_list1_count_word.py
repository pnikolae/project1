import pytest

from topic_05_data_structure.hw.list_1_count_word import *


def test_count_word_ok():
    assert count_word(['1'], '1') == 1
    assert count_word(['1', '2', '1'], '1') == 2
    assert count_word(['1', '2'], '3') == 0


def test_count_word_not_ok():
    assert count_word('1', '1') == 'First arg must be list!'
    assert count_word(['1'], 1) == 'Second arg must be str!'
    assert count_word([], '1') == 'Empty list!'

