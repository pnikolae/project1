from topic_05_data_structure.hw.dict_comprehension_1_pow_start_stop import *


def test_pow_start_stop_wrong_start():
    assert pow_start_stop("1", 4) == 'Start and Stop must be int!'


def test_pow_start_stop_wrong_stop():
    assert pow_start_stop(1, "4") == 'Start and Stop must be int!'


def test_pow_start_stop_bv():
    assert pow_start_stop(1, 1) == {}


def test_pow_start_stop_ok():
    assert pow_start_stop(1, 3) == {1: 1, 2: 4}


def test_pow_start_stop_ok2():
    assert pow_start_stop(2, 5) == {2: 4, 3: 9, 4: 16}
