from topic_05_data_structure.hw.tuple_1_len_count_first import *


def test_len_count_first_not_tuple():
    assert len_count_first(1, '1') == 'First arg must be tuple!'


def test_len_count_first_not_str():
    assert len_count_first((1,), 1) == 'Second arg must be str!'


def test_len_count_first_empty_tuple():
    assert len_count_first((), '1') == 'Empty tuple!'


def test_len_count_first_ok():
    assert len_count_first(('55', 'aa', '66'), '66') == (3, 1, '55')
