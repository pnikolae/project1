from topic_05_data_structure.hw.list_2_magic_mul import *


def test_magic_mul_not_list():
    assert magic_mul(1) == 'Must be list!'


def test_magic_mul_empty_list():
    assert magic_mul([]) == 'Empty list!'


def test_magic_mul_ok():
    assert magic_mul([1,  'aa', 99]) == [1, 1,  'aa', 99, 1,  'aa', 99, 1,  'aa', 99, 99]

