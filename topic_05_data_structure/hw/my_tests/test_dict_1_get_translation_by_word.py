from topic_05_data_structure.hw.dict_1_get_translation_by_word import *


def test_get_translation_by_word_not_dict():
    assert get_translation_by_word([1], 'test') == "Dictionary must be dict!"


def test_get_translation_by_word_not_str():
    assert get_translation_by_word({1:'1'}, 1) == "Word must be str!"


def test_get_translation_by_word_empty_dict():
    assert get_translation_by_word({}, 'test') == "Dictionary is empty!"


def test_get_translation_by_word_empty_str():
    assert get_translation_by_word({1:'1'}, '') == "Word is empty!"


def test_get_translation_by_word_ok():
    word = 'собака'
    dictionary = {'собака': ['dog', 'at']}
    assert get_translation_by_word(dictionary, word) == ['dog', 'at']


def test_get_translation_by_word_no():
    word = 'собака'
    dictionary = {'тест': ['test']}
    string = f"Can't find Russian word: {word}"
    assert get_translation_by_word(dictionary, word) == string
