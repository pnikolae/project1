from topic_05_data_structure.hw.zip_3_car_year import *


def test_zip_car_year_not_list1():
    assert zip_car_year(('Москвич', 'Запорожец'), ['1950', '1960']) == 'Must be list!'


def test_zip_car_year_not_list2():
    assert zip_car_year(['Москвич', 'Запорожец'], ('1950', '1960')) == 'Must be list!'


def test_zip_car_year_empty_list1():
    assert zip_car_year([], ['1950', '1960']) == 'Empty list!'


def test_zip_car_year_empty_list2():
    assert zip_car_year(['Москвич', 'Запорожец'], []) == 'Empty list!'


def test_zip_car_year_ok():
    assert zip_car_year(['Москвич', 'Запорожец'], ['1950', '1960']) == [('Москвич', '1950'),
                                                                        ('Запорожец', '1960')]


def test_zip_car_year_questions1():
    assert zip_car_year(['Москвич', 'Запорожец', 'Волга'], ['1950', '1960']) == [('Москвич', '1950'),
                                                                                 ('Запорожец', '1960'),
                                                                                 ('Волга', "???")]


def test_zip_car_year_questions2():
    assert zip_car_year(['Москвич', 'Запорожец'], ['1950', '1960', "1970"]) == [('Москвич', '1950'),
                                                                                ('Запорожец', '1960'),
                                                                                ("???", "1970")]
