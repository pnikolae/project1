from topic_05_data_structure.hw.set_1_get_info_for_3_set import *


def test_get_info_for_3_set_wrong_set1():
    assert get_info_for_3_set({1: "one"}, {2}, {3}) == 'Must be set!'


def test_get_info_for_3_set_wrong_set2():
    assert get_info_for_3_set({1}, [2], {3}) == 'Must be set!'


def test_get_info_for_3_set_wrong_set3():
    assert get_info_for_3_set({1}, {2}, (3,)) == 'Must be set!'
