from topic_05_data_structure.hw.list_comprehension_2_pow_odd import *


def test_pow_odd_wrong_n():
    assert pow_odd("1") == 'Must be int!'
    assert pow_odd({1, 2, 3}) == 'Must be int!'


def test_pow_odd_bv():
    assert pow_odd(0) == []


def test_pow_odd_bv2():
    assert pow_odd(2) == [1]


def test_pow_odd_ok():
    assert pow_odd(3) == [1]


def test_pow_odd_ok2():
    assert pow_odd(4) == [1, 9]
