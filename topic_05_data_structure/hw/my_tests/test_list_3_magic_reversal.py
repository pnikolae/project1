from topic_05_data_structure.hw.list_3_magic_reversal import *


def test_not_list():
    assert magic_reversal(1) == 'Must be list!'


def test_empty_list():
    assert magic_reversal([]) == 'Empty list!'


def test_magic_reversal_ok():
    assert magic_reversal([1,  'aa', 99]) == ['aa', 'aa', 99, 'aa', 1]
    assert magic_reversal([1, 2]) == [1, 2, 2, 1]
    assert magic_reversal([1]) == [1, 1, 1]


def test_changed_input_list():
    my_list = [1, 2, 3]
    magic_reversal(my_list)
    assert my_list == [1, 2, 3]
