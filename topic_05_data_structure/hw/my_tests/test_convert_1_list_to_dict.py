from topic_05_data_structure.hw.convert_1_list_to_dict import *


def test_list_to_dict_wrong_list():
    assert list_to_dict({1,2,3}, 1) == 'First arg must be list!'


def test_list_to_dict_empty_list():
    assert list_to_dict([],1) == dict()


def test_list_to_dict_bv_yes():
    assert list_to_dict([1], 1) == {1:(0, True, 1)}


def test_list_to_dict_bv_no():
    assert list_to_dict([1], 0) == {1:(0, False, 1)}


def test_list_to_dict_ok():
    assert list_to_dict([1, 2, 3], 0) == {1: (0, False, 3),
                                          2: (1, False, 3),
                                          3: (2, False, 3)}


def test_list_to_dict_ok_repeat():
    assert list_to_dict([1, 2, 1], 0) == {1: (2, False, 2),
                                          2: (1, False, 2)}


def test_list_to_dict_ok_repeat2():
    assert list_to_dict([1, 2, 1], 2) == {1: (2, False, 2),
                                          2: (1, True, 2)}

