from topic_05_data_structure.hw.convert_2_list_to_str import *


def test_list_to_str_wrong_list():
    assert list_to_str({1, 2, 3}, "1") == 'First arg must be list!'


def test_list_to_str_wrong_delimiter():
    assert list_to_str([1, 2, 3], 1) == 'Second arg must be str!'


def test_list_to_str_empty_list():
    assert list_to_str([], "1") == tuple()


def test_list_to_str_bv():
    assert list_to_str([1], "-") == ("1", 0)


def test_list_to_str_ok():
    assert list_to_str([1, 2], "+") == ("1+2", 1)


def test_list_to_str_ok2():
    assert list_to_str([1, 2, 3], "+") == ("1+2+3", 4)
# def test_list_to_str([1, 2], ""):
