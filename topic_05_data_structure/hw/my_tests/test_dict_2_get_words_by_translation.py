from topic_05_data_structure.hw.dict_2_get_words_by_translation import *


def test_get_words_by_translation_not_dict():
    assert get_words_by_translation([1],'dog') == "Dictionary must be dict!"


def test_get_words_by_translation_not_str():
    assert get_words_by_translation({'собака':['dog']}, 1) == "Word must be str!"


def test_get_words_by_translation_empty_dict():
    assert get_words_by_translation({}, 'dog') == "Dictionary is empty!"


def test_get_words_by_translation_empty_word():
    assert get_words_by_translation({'собака':['dog']}, '') == "Word is empty!"


def test_get_words_by_translation_ok():
    my_dict = {'собака': ['dog', 'at'],
               'в': ['in', 'at']
               }
    word = 'at'
    assert get_words_by_translation(my_dict, word) == ['собака', 'в']


def test_get_words_by_translation_no():
    word = 'bird'
    assert get_words_by_translation({'собака': ['dog']}, word) == f"Can't find English word: {word}"
