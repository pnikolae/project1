from topic_05_data_structure.hw.zip_2_colour_shape import *


def test_zip_colour_shape_not_list():
    assert zip_colour_shape(("зеленый", "красный"), ('квадрат', "круг")) == 'First arg must be list!'


def test_zip_colour_shape_not_tuple():
    assert zip_colour_shape(["зеленый", "красный"], ['квадрат', "круг"]) == 'Second arg must be tuple!'


def test_zip_colour_shape_empty_list():
    assert zip_colour_shape([], ('квадрат', "круг")) == 'Empty list!'


def test_zip_colour_shape_empty_tuple():
    assert zip_colour_shape(["зеленый", "красный"], ()) == "Empty tuple!"


def test_zip_colour_shape_ok():
    assert zip_colour_shape(["зеленый", "красный"], ('квадрат', "круг")) == [("зеленый", 'квадрат'),
                                                                             ("красный", "круг")]
