from topic_05_data_structure.hw.zip_1_names import *


def test_zip_names_not_list():
    assert zip_names(('Иван',), {'Петров'}) == 'First arg must be list!'


def test_zip_names_not_set():
    assert zip_names(['Иван'], ['Петров']) == 'Second arg must be set!'


def test_zip_names_empty_list():
    assert zip_names([], {'Петров'}) == 'Empty list!'


def test_zip_names_empty_set():
    assert zip_names(['Иван'], set()) == 'Empty set!'


def test_zip_names_ok():
    assert zip_names(['Иван'], {'Петров'}) == [("Иван", "Петров")]
