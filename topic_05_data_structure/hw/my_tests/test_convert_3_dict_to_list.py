from topic_05_data_structure.hw.convert_3_dict_to_list import *


def test_dict_to_list_wrong_dict():
    assert dict_to_list({1, 2, 3}) == 'Must be dict!'


def test_dict_to_list_empty_dict():
    assert dict_to_list({}) == ([], [], 0, 0)


def test_dict_to_list_1elem():
    assert dict_to_list({1: "one"}) == ([1], ["one"], 1, 1)


def test_dict_to_list_2elems():
    assert dict_to_list({1: "one", 2: "two"}) == ([1, 2], ["one", "two"], 2, 2)


def test_dict_to_list_3elems():
    assert dict_to_list({1: "one", 2: "two", 1: "three"}) == ([1, 2], ["three", "two"], 2, 2)


def test_dict_to_list_repeat():
    assert dict_to_list({1: "one", 2: "two", 3: "one"}) == ([1, 2, 3], ["one", "two", "one"], 3, 2)
# def test_dict_to_list():
