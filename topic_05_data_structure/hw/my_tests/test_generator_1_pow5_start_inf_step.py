from topic_05_data_structure.hw.generator_1_pow5_start_inf_step import *


def test_pow5_start_inf_step_wrong_start():
    assert pow5_start_inf_step("a", 1) == 'Start and Step must be int!'


def test_pow5_start_inf_step_wrong_step():
    assert pow5_start_inf_step(1, "a") == 'Start and Step must be int!'


def test_pow5_start_inf_step_0():
    gen_expr = pow5_start_inf_step(0, 0)
    assert next(gen_expr) == 0
    assert next(gen_expr) == 0
    assert next(gen_expr) == 0


def test_pow5_start_inf_step_neg_step():
    gen_expr = pow5_start_inf_step(3, -1)
    assert next(gen_expr) == 243
    assert next(gen_expr) == 32
    assert next(gen_expr) == 1
    assert next(gen_expr) == 0
    assert next(gen_expr) == -1


def test_pow5_start_inf_step_ok():
    gen_expr = pow5_start_inf_step(1, 1)
    assert next(gen_expr) == 1
    assert next(gen_expr) == 32
    assert next(gen_expr) == 243
