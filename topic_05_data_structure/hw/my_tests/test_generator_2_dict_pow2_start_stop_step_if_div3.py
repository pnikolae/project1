from topic_05_data_structure.hw.generator_2_dict_pow2_start_stop_step_if_div3 import *


def test_dict_pow2_start_stop_step_if_div3_wrong_start():
    assert dict_pow2_start_stop_step_if_div3(2.2, 10, 1) == 'Start and Stop and Step must be int!'


def test_dict_pow2_start_stop_step_if_div3_wrong_stop():
    assert dict_pow2_start_stop_step_if_div3(2, "a", 10) == 'Start and Stop and Step must be int!'


def test_dict_pow2_start_stop_step_if_div3_wrong_step():
    assert dict_pow2_start_stop_step_if_div3(2, 10, [1]) == 'Start and Stop and Step must be int!'


def test_dict_pow2_start_stop_step_if_div3_0_step():
    assert dict_pow2_start_stop_step_if_div3(2, 10, 0) == "Step can't be zero!"


def test_dict_pow2_start_stop_step_if_div3_bv():
    gen_expr = dict_pow2_start_stop_step_if_div3(1, 2, 1)
    assert gen_expr  # next(gen_expr) == tuple()


def test_dict_pow2_start_stop_step_if_div3_ok():
    gen_expr = dict_pow2_start_stop_step_if_div3(1, 4, 1)
    assert next(gen_expr) == (3, 9)


def test_dict_pow2_start_stop_step_if_div3_ok2():
    gen_expr = dict_pow2_start_stop_step_if_div3(0, 4, 1)
    assert next(gen_expr) == (0, 0)
    assert next(gen_expr) == (3, 9)

