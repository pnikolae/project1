"""
Функция magic_mul.

Принимает 1 аргумент: список my_list.

Возвращает список, который состоит из
[первого элемента my_list]
+ [три раза повторенных списков my_list]
+ [последнего элемента my_list].

Пример:
входной список [1,  'aa', 99]
результат [1, 1,  'aa', 99, 1,  'aa', 99, 1,  'aa', 99, 99].

Если вместо списка передано что-то другое, то возвращать строку 'Must be list!'.
Если список пуст, то возвращать строку 'Empty list!'.
"""


def magic_mul(my_list):
    if type(my_list) != list:
        return 'Must be list!'
    if not len(my_list):
        return 'Empty list!'

    new_list = [my_list[0]]
    new_list.extend(my_list * 3)
    new_list.append(my_list[-1])

    return new_list


if __name__ == '__main__':
    print(magic_mul(1))
    print(magic_mul([]))
    print(magic_mul([1]))
    #print(magic_mul())