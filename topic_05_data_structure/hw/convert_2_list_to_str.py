"""
Функция list_to_str.

Принимает 2 аргумента: список и разделитель (строка).

Возвращает (строку полученную разделением элементов списка разделителем,
количество разделителей в получившейся строке в квадрате).

Если вместо списка передано что-то другое, то возвращать строку 'First arg must be list!'.

Если разделитель не является строкой, то возвращать строку 'Second arg must be str!'.

Если список пуст, то возвращать пустой tuple().

ВНИМАНИЕ: в списке могут быть элементы любого типа (нужно конвертировать в строку).
"""


def list_to_str(my_list: list, delimiter: str):
    if type(my_list) != list:
        return 'First arg must be list!'

    if type(delimiter) != str:
        return 'Second arg must be str!'

    if not my_list:
        return tuple()

    str_list = []
    for elem in my_list:
        str_list.append(str(elem))
    new_str = delimiter.join(str_list)
    return new_str, new_str.count(delimiter) ** 2


if __name__ == '__main__':
    t = ["test1"]
    print("_".join(t))

    print(list_to_str({1, 2}, "1"))
    print(list_to_str([1, 2], 1))
    print(list_to_str([], "1"))
    print(list_to_str([1], "+"))
    print(list_to_str([1, 2], "+"))
    print(list_to_str([1, 2, 3], "+"))

