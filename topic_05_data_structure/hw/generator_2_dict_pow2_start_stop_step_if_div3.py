"""
Функция dict_pow2_start_stop_step_if_div3.

Принимает 3 аргумента: числа start, stop, step.

Возвращает генератор-выражение состоящий из кортежа (аналог dict):
(значение, значение в квадрате)
при этом значения перебираются от start до stop (не включая) с шагом step
только для чисел, которые делятся на 3 без остатка.

Пример: start=0, stop=10, step=1 результат ((0, 0), (3, 9), (6, 36), (9, 81)).

Если start или stop или step не являются int, то вернуть строку 'Start and Stop and Step must be int!'.
Если step равен 0, то вернуть строку "Step can't be zero!"
"""


def dict_pow2_start_stop_step_if_div3(start, stop, step):
    if type(start) != int or \
            type(stop) != int or \
            type(step) != int:
        return 'Start and Stop and Step must be int!'
    if 0 == step:
        return "Step can't be zero!"
    return ((x, x*x) for x in range(start, stop, step) if 0 == x % 3)


if __name__ == '__main__':
    print(dict_pow2_start_stop_step_if_div3("a", 10, 1))
    print(dict_pow2_start_stop_step_if_div3(0, "10", 1))
    print(dict_pow2_start_stop_step_if_div3(0, 10, "1"))
    print(dict_pow2_start_stop_step_if_div3(0, 10, 0))
    gen = dict_pow2_start_stop_step_if_div3(0, 10, 1)
    print(next(gen))
    print(next(gen))
    print(next(gen))
    print(next(gen))
