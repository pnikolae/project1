"""
Функция zip_names.

Принимает 2 аргумента: список с именами и множество с фамилиями.

Возвращает список с парами значений из каждого аргумента.

Если вместо list передано что-то другое, то возвращать строку 'First arg must be list!'.
Если вместо set передано что-то другое, то возвращать строку 'Second arg must be set!'.

Если list пуст, то возвращать строку 'Empty list!'.
Если set пуст, то возвращать строку 'Empty set!'.

Если list и set различного размера, обрезаем до минимального (стандартный zip).
"""


def zip_names(names: list, surnames: set):
    if type(names) != list:
        return 'First arg must be list!'
    if type(surnames) != set:
        return 'Second arg must be set!'
    if 0 == len(names):
        return 'Empty list!'
    if 0 == len(surnames):
        return 'Empty set!'

    return list(zip(names, surnames))

if __name__ == '__main__':
    print(zip_names(['Иван', 'Петр'], {'Петров', 'Иванов'}))
